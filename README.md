# purgeR [![build status](https://gitlab.com/elcortegano/purgeR/badges/master/pipeline.svg)](https://gitlab.com/elcortegano/purgeR/commits/master) [![coverage](https://gitlab.com/elcortegano/purgeR/badges/master/coverage.svg)](https://gitlab.com/elcortegano/purgeR/commits/master) [![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/purgeR)](https://cran.r-project.org/web/packages/purgeR/index.html) [![downloads](https://cranlogs.r-pkg.org/badges/grand-total/purgeR)](https://cranlogs.r-pkg.org/badges/grand-total/purgeR) [![License](https://img.shields.io/badge/license-GPL--2-black)](https://cran.r-project.org/web/licenses/GPL-2)
Estimation of inbreeding-purging genetic parameters in pedigreed populations.

## Overview

purgeR is an R package that includes functions for the computation of parameters related to inbreeding and genetic purging in pedigreed populations, including standard, ancestral and purged inbreeding coefficients, among other measures of inbreeding and purging. In addition, functions to compute the effective population size and other parameters relevant to population genetics and structure are included.

## Installation

Install the R package from CRAN as follows:

```
install.packages("purgeR")
```

The development version can be installed from GitLab via the [remotes](https://remotes.r-lib.org/) package:

```
remotes::install_gitlab("elcortegano/purgeR")
```

## Quick tutorial

A complete user's guide with examples is provided as vignettes, introducing functions in this package and providing examples of use. Navigate these vignettes from R with:

```
browseVignettes("purgeR")
```

There are currently two vignettes available:

- **purgeR-tutorial**: A complete overview of all functions in the package, including easy to follow examples.

- **ip**: A more advanced guide showing examples of inbreeding purging analyses.

## Getting help

Users are encouraged to report bugs and request additional features. Please use the [issue tracker at GitLab](https://gitlab.com/elcortegano/purgeR/issues) to do so (it will require an account).

## Contributing

This is an open source project open to collaboration. Developers are welcome to [contact me](mailto:elcortegano@gmail.com) and contribute with code to this project. 

## Citation

If you use purgeR in your research, please cite:  

 - López-Cortegano E. (2021). Inbreeding and purging in pedigreed populations. *Bioinformatics*, doi: [https://doi.org/10.1093/bioinformatics/btab599.](https://doi.org/10.1093/bioinformatics/btab599)  
